local socket = require "socket"
local json = require "json"

local DEBUG = false

local function LOGD(msg, params)
   if DEBUG then
      print(string.format(msg, params))
   end
end

local NoobBot = {}
NoobBot.__index = NoobBot

function NoobBot.create(conn, name, key)
   local bot = {}
   setmetatable(bot, NoobBot)
   bot.conn = conn
   bot.name = name
   bot.key = key
   bot.driver = require "driver"
   bot.turboFactor = 1
   bot.racePhase = 0
   return bot
end

function NoobBot:msg(msgtype, data)
   
   return json.encode.encode({msgType = msgtype, data = data, gameTick=self.driver.gameTick})
   
end

function NoobBot:send(msg)
   LOGD("Sending msg: %s", msg)
   self.conn:send(msg .. "\n")
end

function NoobBot:join()
   return self:msg("join", {name = self.name, key = self.key})
end

function NoobBot:createrace()
   local idtable = {name = self.name, key=self.key}
   return self:msg("joinRace", {trackName = "france", carCount = 1, botId=idtable,password="test"})
end

function NoobBot:onyourcar(data)
   self.driver.name = data.name
end

function NoobBot:ongameinit(data)
   self.finished = false
   if self.racePhase == 1 then
      print("Race phase")
      self.driver.race = true
      self.driver.turboFactor = 1
      self.driver.turboTick = nil
      self.driver.turboDuration = nil
      self.racePhase = 2
   elseif self.racePhase == 0 then
      self.driver:setTrackInfo(data)
      self.driver.race = false
      self.racePhase = 1
      print("Qual phase")
   end
end

function NoobBot:throttle(throttle)
   return self:msg("throttle", throttle)
end

function NoobBot:switchlane(direction)
   return self:msg("switchLane", direction)
end

function NoobBot:ondnf()
end

function NoobBot:onfinish(data)
   if data.name == self.driver.name then
      self.finished = true
   end
end

function NoobBot:onlapfinished(data)
end

function NoobBot:ping()
   return self:msg("ping", {})
end

function NoobBot:run()
   --self:send(self:createrace())
   self:send(self:join())
   self:msgloop()
end

function NoobBot:onjoin(data)
   print("Joined")
   --self:send(self:ping())
end

function NoobBot:ongamestart(data)
   print("Race started")
   self:oncarpositions(self.savedCarPos)
   self.savedCarPos = nil
end

function NoobBot:onturboavailable(data)
   if not self.crashed then
      self.turboFactor = data.turboFactor
      self.turboTick = self.driver.gameTick
      self.turboDuration = data.turboDurationTicks
   end
   print("Got turbo")
end

function NoobBot:onturbostart(data)
   if data.name == self.driver.name then
      self.driver.turboFactor = self.turboFactor
      self.turboFactor = 1
      print("Started turbo")
   end
end

function NoobBot:onturboend(data)
   self.driver.turboFactor = 1
   self.driver.turboTick = nil
   self.driver.turboDuration = nil
end

function NoobBot:turbo()
   return self:msg("turbo", "Indeed.")
end

function NoobBot:oncarpositions(data)
   if self.finished then
      self:send(self:ping())
      return
   end

   if self.driver.gameTick then
      self.driver:setCarPositions(data)
      if self.turboFactor ~= 1 then
         local turbo = self.driver:turboActivation()
         if turbo then
            self:send(self:turbo())
            --self.driver:saveLogInfo()
            return
         end
      end
      local instructions = self.driver:getInstructions()
      if instructions.type == "throttle" then
         self:send(self:throttle(instructions.value))
      elseif instructions.type == "switch" then
         self:send(self:switchlane(instructions.value))
      end
      --self.driver:saveLogInfo()
   else
      self.savedCarPos = data
   end
end

function NoobBot:oncrash(data)
   print("Someone crashed")
   if data.name == self.driver.name then
      self.crashed = true
      self.turboFactor = 1
      self.driver.turboFactor = 1
      self.driver.turboTick = nil
      self.driver.turboDuration = nil
      if not self.driver.bumpTarget then
         self.driver:didCrash()
      else
         --Crashed after trying to bump, don't adjust corner values
         self.driver.crashed = true
      end
   elseif data.name == self.driver.bumpTarget then
      self.driver:bumpSucceeded()
   end
   --self:send(self:ping())
end

function NoobBot:onspawn(data)
   if data.name == self.driver.name then
      self.crashed = false
   end
end

function NoobBot:ongameend(data)
   print("Race ended")
   self.driver:cleanVariables()
   --self:send(self:ping())
end

function NoobBot:onerror(data)
   print("Error: " .. data)
   --self:send(self:ping())
end

function NoobBot:msgloop()
   local line = self.conn:receive("*l")
   --while line ~= nil or #len > 0 do
   while line ~= nil do
      LOGD("Got message: %s", line)
      local msg = json.decode.decode(line)
      if msg then
   	 local msgtype = msg["msgType"]
   	 local fn = NoobBot["on" .. string.lower(msgtype)]
   	 if fn then
          self.driver.gameTick = msg.gameTick
   	    fn(self, msg["data"])
   	 else
   	    print("Got " .. msgtype)
   	    --self:send(self:ping())
   	 end
      end
      line = self.conn:receive("*l")
   end
end

if #arg == 4 then
   local host, port, name, key = unpack(arg)
   print("Connecting with parameters:")
   print(string.format("host=%s, port=%s, bot name=%s, key=%s", unpack(arg)))
   local c = assert(socket.connect(host, port))
   bot = NoobBot.create(c, name, key)
   bot:run()
else
   print("Usage: ./run host port botname botkey")
end
