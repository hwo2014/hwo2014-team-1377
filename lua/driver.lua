--Bot with a pretty simple idea but a messy execution. Learns how fast to drive in qualifying by going slightly
--faster in each different corner if the slip angle is not large enough.
--In race the bot uses the lanes it wants to and just tries to bump other players out of the way.
--Only looks for people who are in the same piece to bump out of the track. Also doesn't take other
--players bumping me into consideratin in any way.
local driver = {}

driver.curPiece = nil
driver.cornerValue = 0.45 --Initial cornering value (v^2/r) to start adjusting from
driver.curVelocity = 0
driver.breakCoef = 0.98
driver.enginePower = 0.2
driver.turboFactor = 1
driver.throttle = 1
driver.ticksInAngle = 0
driver.maxStraight = nil
driver.angle = nil
driver.prevAngle = nil
driver.velocityReached = nil
driver.cornerAngleMax = nil
driver.cornerStart = nil
driver.curDist = nil --Current inPieceDistance
driver.crashed = false
driver.first = nil
driver.second = nil

function driver:findThrottle(curSpeed, idealSpeed)
	return (idealSpeed - driver.breakCoef*curSpeed)/(driver.enginePower*driver.turboFactor)
end

--Save track info. Calculate some total corner angles and straight lengths.
function driver:setTrackInfo(data)
	self.track = data.race.track

	local rightTurns = 0
	local leftTurns = 0
	local totalAngle = 0
	local firstStraight = false
	local startNum = 0
	local maxStraight = 0
	local isStraight = false
	for i=1,#self.track.pieces do
		if self.track.pieces[i].radius then
			if self.track.pieces[i].angle > 0 then
				if rightTurns == 0 then
					leftTurns = 0
					totalAngle = 0
					startNum = i
				end
				rightTurns = rightTurns + 1
				totalAngle = totalAngle + self.track.pieces[i].angle
			else
				if leftTurns == 0 then
					rightTurns = 0
					totalAngle = 0
					startNum = i
				end
				leftTurns = leftTurns + 1
				totalAngle = totalAngle + self.track.pieces[i].angle
			end

			for j=startNum,i do
				self.track.pieces[j].totalAngle = totalAngle
			end
			
			firstStraight = true
		else
			firstStraight = false
			if not firstStraight then
				rightTurns = 0
				leftTurns = 0
				totalAngle = 0
			end
			local nextPiece = i
			local thisStraight = self.track.pieces[i].length
			isStraight = true
			while isStraight do
				nextPiece = nextPiece + 1
				if nextPiece > #self.track.pieces then
					nextPiece = 1
				end
				if not self.track.pieces[nextPiece].radius then
					thisStraight = thisStraight + self.track.pieces[nextPiece].length
				else
					isStraight = false
				end
			end
			self.track.pieces[i].straightLength = thisStraight
			if thisStraight > maxStraight then
				maxStraight = thisStraight
			end
		end
	end
	self.maxStraight = maxStraight
end

function driver:setCarPositions(data)

	for i=1,#data do
		if data[i].id.name == self.name then
			self.carPos = data[i]
			self.ownIndex = i
		end
	end
	self.allCarPos = data
	self.prevAngle = self.angle
	self.angle = self.carPos.angle
	
	local nextPiece
	if not self.curPiece or self.curPiece ~= self.carPos.piecePosition.pieceIndex + 1 then
		--If the qualifying starts from a corner section, make sure the program is initialized
		--properly
		if not self.curPiece and self.track.pieces[self.carPos.piecePosition.pieceIndex+1].radius then
			self:calculateCornerInfo(self.carPos.piecePosition.pieceIndex+1)
		end

		self:updateCornerData()
		self:handleCornerAdjustments(self.carPos.piecePosition.pieceIndex + 1)
		self.newPiece = true
		self.curPiece = self.carPos.piecePosition.pieceIndex + 1
		if self.curDist then
			--Using estimated velocity based on throttle because calculations at some
			--piece changes are not accurate for some reason (switch pieces maybe)
			--May not be accurate in case of someone bumping on me but whatever
			self.curVelocity = self.curVelocity*self.breakCoef + self.throttle*self.enginePower*self.turboFactor
		end
		--Start recording data about cornering speeds for this corner
		--If we were trying to bump someone, remove the data once
		--the right corner stars
		if self.track.pieces[self.curPiece].radius and not self.cornerStart then
			self.velocityReached = false
			self.cornerStart = self.curPiece
			self:updateCornerData()
			if self.bumpingCorner and self.curPiece == self.bumpingCorner then
				self.bumpingCorner = nil
				self.bumpTarget = nil
			end
		end
		--For each rather straight segment check that we are not still going slow because
		--of a previous bump, and restart the bumpingtargets (need to check again if they are
			--right in front)
		if not self.track.pieces[self.curPiece].radius or math.abs(self.track.pieces[self.curPiece].angle) < 30 or self.curPiece == 1 then
			if self.goSlowMo then
				if not self.slowMoTick or self.gameTick - self.slowMoTick >= 5 then
					self.goSlowMo = false
					self.slowMoTick = nil
				end
			end
			self.bumpTarget = nil
			self.bumpingCorner = nil
		end
	else
		self.newPiece = false
		if self.curDist then
			local oldVel = self.curVelocity
			self.curVelocity = self.carPos.piecePosition.inPieceDistance - self.curDist

			--Out of the first two ticks, record enginepower and the drag coefficient that
			--slows us down
			if self.first and not self.second then
				self.breakCoef = (self.curVelocity-self.throttle*self.enginePower*self.turboFactor)/oldVel
				self.second = true
				--print("Break: " .. self.breakCoef)
			end
			if not self.first and self.curVelocity > 0 then
				self.enginePower = self.curVelocity/self.throttle
				self.first = true
				--print("Engine: " .. self.enginePower)
			end
			--If we bumped someone, slow down a bit for the next corner
			if self.bumpTarget and self.curVelocity - oldVel < -2 then
				if self.curVelocity > 4 or math.abs(self.angle) > 30 then
					self.goSlowMo = true
				end
				self.bumpTick = self.gameTick
				self.bumpingCorner = nil
				self.bumpTarget = nil
			elseif not self.track.pieces[self.curPiece].radius then
				--For each straight segment check if we can bump someone
				--of the track
				if not self.bumpingCorner then
					if not self.bumpTick or self.gameTick - self.bumpTick > 5 then
						if self:isThereACar("Front") then
							self:findBumpingCorner()
						end
					end
				end
				--If we have been bumped into, slow down
			elseif self.curVelocity - oldVel > 3 + self.enginePower*self.throttle*self.turboFactor then
				self.slowMoTick = self.gameTick
				self.goSlowMo = true
				--If in corner and may be going too fast because of trying to bump,
				--just let go and wait a bit before trying again.
			elseif self.bumpTarget and math.abs(self.angle) > 35 then
				self.bumpTick = self.gameTick
				self.bumpingCorner = nil
				self.bumpTarget = nil
				self.goSlowMo = true
			end
		end
		
		self:updateCornerData()

	end
	self.curDist = self.carPos.piecePosition.inPieceDistance
end

--Update data relating to how fast we are going around a corner and what is the sliding angle.
function driver:updateCornerData()
	if not self.cornerStart then
		return
	end
	self.ticksInAngle = self.ticksInAngle + 1
	if not self.cornerAngleMax or self.cornerAngleMax < math.abs(self.angle) then
		if self.curVelocity >= self.track.pieces[self.curPiece].distChange and self.ticksInAngle < 2 then
			self.velocityReached = true
		end
		if self.angle*self.track.pieces[self.curPiece].angle > 0 then
			self.cornerAngleMax = math.abs(self.angle)
		else
			self.cornerAngleMax = 0
		end
	end
	--Save info about the last corner combination in case we crash right after
	if self.prevCorner then
		if self.prevCorner[3] < 5 then
			self.prevCorner[3] = self.prevCorner[3] + 1
		else
			self.prevCorner = nil
		end
	end
end

--Adjust speed parameter for the last combination of corners.
--Go faster if we didn't slide much last time around, take also into account
--whether the sliding angle was growing when the corner ended and whether
--the next piece is a long straight.
--Specific values were adjusted manually so that the car would learn slowly
--to go quicker during qualifying.
function driver:handleCornerAdjustments(nextPiece)
	local finishCorner = false
	if not self.cornerStart then
		return
	end
	if not self.track.pieces[nextPiece].radius then
		finishCorner = true
	else
		if math.abs(self.track.pieces[nextPiece].angle) > math.abs(self.track.pieces[self.cornerStart].angle) then
			finishCorner = true
		end
	end
	if finishCorner then
		self.ticksInAngle = 0
		--print(self.cornerAngleMax,math.abs(self.angle) - math.abs(self.prevAngle))
		local cornerValue = self.track.pieces[self.curPiece].cornerValue
		local old = cornerValue
		local angleDiff = math.abs(self.angle) - math.abs(self.prevAngle)
		if not self.velocityReached or self.crashed or self.race then
			self.crashed = false
		elseif self.cornerAngleMax < 15 then
			if angleDiff < 6 then
				cornerValue = 1.3*cornerValue
			elseif angleDiff < 9 then
				if self.track.pieces[nextPiece].straightLength and self.track.pieces[nextPiece].straightLength > 200 then
					cornerValue = 1.2*cornerValue
				else
					cornerValue = 1.05*cornerValue
				end
			end
			
		elseif self.cornerAngleMax < 30 then 
			if angleDiff < 1.5 then
				cornerValue = 1.35*cornerValue
			elseif angleDiff < 4 then
				if self.track.pieces[nextPiece].straightLength and self.track.pieces[nextPiece].straightLength > 200 then
					cornerValue = 1.2*cornerValue
				else
					cornerValue = 1.05*cornerValue
				end
			elseif angleDiff < 6 then
				if self.track.pieces[nextPiece].straightLength and self.track.pieces[nextPiece].straightLength > 200 then
					cornerValue = 1.15*cornerValue
				else
					cornerValue = 1.01*cornerValue
				end
			end
		elseif self.cornerAngleMax < 45 then
			if angleDiff < 1.5 then
				cornerValue = 1.08*cornerValue
			elseif angleDiff < 3 then
				if self.track.pieces[nextPiece].straightLength and self.track.pieces[nextPiece].straightLength >= 200 then
					cornerValue = 1.06*cornerValue
				else
					cornerValue = 1.03*cornerValue
				end
			end
			
		elseif self.cornerAngleMax < 53 then
			if angleDiff < 1.5 then
				cornerValue = 1.035*cornerValue
			elseif angleDiff < 2.5 then
				if self.track.pieces[nextPiece].straightLength and self.track.pieces[nextPiece].straightLength >= 200 then
					cornerValue = 1.02*cornerValue
				else
					cornerValue = 1.015*cornerValue
				end
			end
		elseif self.cornerAngleMax < 56 then
			if angleDiff < 1.5 then
				cornerValue = 1.01*cornerValue
			end
		elseif self.cornerAngleMax < 58 then 
			if angleDiff < 1.5 then
				cornerValue = 1.005*cornerValue
			end
		elseif self.cornerAngleMax < 59 then
			if angleDiff < 0 then
				cornerValue = 1.005*cornerValue
			end
		end
		if cornerValue > old then
			--print("IMPROV!")
		end
		for i=self.cornerStart, self.curPiece do
			self.track.pieces[i].cornerValue = cornerValue
		end
		self.prevCorner = {self.cornerStart, self.curPiece,0}
		self.cornerStart = nil
		self.cornerAngleMax = nil
	end
end

function driver:bumpSucceeded()
	--print("BUMP SUCCESS!")
	self.bumpTarget = nil
end
--In the case of a crash, make a note to go slower to that corner the next time.
function driver:didCrash()
	self.crashed = true
	if self.prevCorner then
		local ind = self.prevCorner[1]
		local sameCorner = true
		while sameCorner do
			self.track.pieces[ind].cornerValue = 0.95*self.track.pieces[ind].cornerValue
			if ind == self.prevCorner[2] then
				sameCorner = false
			end
			ind = ind + 1
			if ind > #self.track.pieces then
				ind = 1
			end
		end
		--print("prevCorner crash", self.prevCorner[1], self.prevCorner[2])
		self.prevCorner = nil
	else
		if self.cornerStart then
			local sameCorner = true
			local ind = self.cornerStart
			while sameCorner do
				--print("Crash correction: " .. ind)
				self.track.pieces[ind].cornerValue = 0.95*self.track.pieces[ind].cornerValue
				ind = ind + 1
				if ind > #self.track.pieces then
					ind = 1
				end
				if not self.track.pieces[ind].angle or (math.abs(self.track.pieces[ind].angle) > math.abs(self.track.pieces[self.cornerStart].angle)) then
					sameCorner = false
				end
			end
		end
	end
end

function driver:turboActivation()
	local pieceCounter = self.curPiece
	local straightLength = 0
	local counting = true
	local kindOfStraightLength = 0

 	if self.track.pieces[self.curPiece].angle then
 		return false
 	end

	while counting do
		if not self.track.pieces[pieceCounter].radius then
			straightLength = straightLength + self.track.pieces[pieceCounter].length
			kindOfStraightLength = kindOfStraightLength + self.track.pieces[pieceCounter].length
		else
			if math.abs(self.track.pieces[pieceCounter].angle) > 25 then
				counting = false
			elseif math.abs(self.track.pieces[pieceCounter].angle) > 10 then
				kindOfStraightLength = kindOfStraightLength + self.track.pieces[pieceCounter].length
			else
				kindOfStraightLength = kindOfStraightLength + self.track.pieces[pieceCounter].length
				straightLength = straightLength + self.track.pieces[pieceCounter].length
			end
		end
		pieceCounter = pieceCounter + 1
		if pieceCounter > #self.track.pieces then
			pieceCounter = 1
		elseif pieceCounter == self.curPiece then
			counting = false
		end
	end
	
	if straightLength/self.maxStraight > 0.9 then
		return true
	elseif kindOfStraightLength > 300 and not self.bumpingCorner then
		if not self.bumpTick or self.gameTick - self.bumpTick > 5 then
			if self:isThereACar("Front") then 
				--print("TURBO BUMP")
				self:findBumpingCorner()
				return true
			end
		end
	else
		return false
	end
end

function driver:findBumpingCorner()
	local curPiece = self.curPiece
	local notFound = true
	while notFound do
		curPiece = curPiece + 1
		if curPiece > #self.track.pieces then
			curPiece = 1
		elseif curPiece == self.curPiece then
			notFound = false
		end
		if self.track.pieces[curPiece].angle and (math.abs(self.track.pieces[curPiece].angle) >= 45 or math.abs(self.track.pieces[curPiece].totalAngle) >= 75) then
			notFound = false
			self.bumpingCorner = curPiece
		end
	end
end
--Function to find the required throttle value or switch value.
function driver:getInstructions()
	local instructions = {}
	self.targetCorner = nil
	if self.newPiece then
		local switchDir = self:decideToSwitch()
		if switchDir then
			instructions.type = "switch"
			instructions.value = switchDir
			return instructions
		end
	end

	instructions.type = "throttle"
	self:findBreakingCorner()
	if self.targetCorner then
		instructions.value = self:findThrottle(self.curVelocity, self.track.pieces[self.targetCorner].distChange)
	else
		if self.track.pieces[self.curPiece].radius then
			instructions.value = self:findThrottle(self.curVelocity, self.track.pieces[self.curPiece].distChange)
		else
			instructions.value = 1
		end
	end
	if instructions.value > 1 then
		instructions.value = 1
	elseif instructions.value < 0 then
		instructions.value = 0
	end
	self.throttle = instructions.value
	return instructions
end

--Switch if between this switch and the next one we can drive more corners on the
--inside track.
function driver:decideToSwitch()
	local nextPiece = self.curPiece + 1
	if nextPiece > #self.track.pieces then
		nextPiece = 1
	end
	if self.track.pieces[nextPiece].switch then
		self.switchPossible = true
		local foundNextSwitch = false
		local nextSwitchInd = nextPiece + 1
		local turnCounter = 0
		while not foundNextSwitch do
			if nextSwitchInd > #self.track.pieces then
				nextSwitchInd = 1
			end
			
			if self.track.pieces[nextSwitchInd].radius then
				if self.track.pieces[nextSwitchInd].angle > 0 then
					turnCounter = turnCounter + 1
				else
					turnCounter = turnCounter - 1
				end
			end
			if self.track.pieces[nextSwitchInd].switch then
				foundNextSwitch = true
			end
			nextSwitchInd = nextSwitchInd + 1
		end
		if turnCounter > 0 and self.carPos.piecePosition.lane.startLaneIndex < #self.track.lanes - 1  then
			--return nil
			return "Right"
		elseif turnCounter < 0 and self.carPos.piecePosition.lane.startLaneIndex > 0 then
			--return nil
			return "Left"
		end
	else
		return nil
	end
end

--Elementary function to check whether there is a car in the given direction.
--Only checks cars in the same piece.
function driver:isThereACar(direction)
	for i=1, #self.allCarPos do
		if i ~= self.ownIndex then
			local carPos = self.allCarPos[i].piecePosition
			local inPieceDist = carPos.inPieceDistance
			local piece = carPos.pieceIndex + 1
			local startLane = carPos.lane.startLaneIndex
			local endLane = carPos.lane.endLaneIndex
			local distance 
			local minDistance = 150
			if self.curPiece == piece then
				distance = self.curDist - inPieceDist
			end
			if direction == "Front" then
				if distance and distance > -100 and distance < 0 then
					if endLane == self.carPos.piecePosition.lane.endLaneIndex then
						if math.abs(distance) < minDistance then
							self.bumpTarget = self.allCarPos[i].id.name
							minDistance = math.abs(distance)
						end
					end
				end
			elseif direction == "Left" then
				if distance and math.abs(distance) < 40 then
					if startLane == self.carPos.piecePosition.lane.startLaneIndex - 1 then
						return true
					end
				end
			elseif direction == "Right" then
				if distance and math.abs(distance) < 40 then
					if startLane == self.carPos.piecePosition.lane.startLaneIndex + 1 then
						return true
					end
				end
			end
		end
	end
	if direction == "Front" and self.bumpTarget then
		--print("TARGET: " .. self.bumpTarget)
		return true
	end
	return false
end

--Calculate distance from current position to the start of the given piece.
function driver:calculateDistance(pieceNum)
	local distance = self.track.pieces[self.curPiece].length - self.carPos.piecePosition.inPieceDistance
	local pieceCounter = self.curPiece
	local counting = true

	while counting do
		pieceCounter = pieceCounter + 1
		if pieceCounter > #self.track.pieces then
			pieceCounter = 1
		end

		if pieceCounter == pieceNum then
			--distance = distance + self.track.pieces[pieceCounter].length/2
			counting = false
		else
			distance = distance + self.track.pieces[pieceCounter].length
		end
	end
	return distance
end

--Look some amount of pieces ahead to see whether we have to break to a corner.
--In the race break a bit earlier to make sure we don't fall off the track.
function driver:findBreakingCorner()
	local curInd = self.curPiece
	local piecesAhead = 8
	for i=1,piecesAhead do
		local checkPiece = curInd + i
		if checkPiece > #self.track.pieces then
			checkPiece = checkPiece - #self.track.pieces
		end
		if self.track.pieces[checkPiece].radius then
			self:calculateCornerInfo(checkPiece)
			local curSpeed = self.curVelocity
			local targetSpeed = self.track.pieces[checkPiece].distChange
			local nextSpeed
			if self.track.pieces[self.curPiece].radius then
				nextSpeed = curSpeed
			else
				nextSpeed = self.enginePower*self.turboFactor + self.breakCoef*curSpeed
			end
			local distance = self:calculateDistance(checkPiece) - nextSpeed
			local ticks = math.ceil(math.log(targetSpeed/nextSpeed)/math.log(self.breakCoef))
			local breakDist = nextSpeed/(1-self.breakCoef)*(1-math.pow(self.breakCoef,ticks-1))
			
			if ticks > 1 and distance - breakDist <= 0 then
				self.targetCorner = checkPiece
			end
			if checkPiece == self.bumpingCorner then
				break
			end
		end

	end
end

--Calculate relevant information for the corner to decide how fast we can go
function driver:calculateCornerInfo(pieceNum)

	if self.track.pieces[pieceNum].radius then
		local cornerRadius
		if self.track.pieces[pieceNum].angle > 0 then
			cornerRadius = self.track.pieces[pieceNum].radius - self.track.lanes[self.carPos.piecePosition.lane.endLaneIndex+1].distanceFromCenter
		else
			cornerRadius = self.track.pieces[pieceNum].radius + self.track.lanes[self.carPos.piecePosition.lane.endLaneIndex+1].distanceFromCenter
		end
		--Go slightly slower in the race to avoid crashing if possible
		if self.race then
			cornerRadius = 0.95*cornerRadius
		end
		self.track.pieces[pieceNum].length = 2*cornerRadius*math.pi*math.abs(self.track.pieces[pieceNum].angle)/360
		if not self.track.pieces[pieceNum].cornerValue then
			self.track.pieces[pieceNum].cornerValue = self.cornerValue
		end
		if self.bumpingCorner and self.bumpingCorner == pieceNum then
			--print("Bump: " .. self.bumpingCorner)
			self.track.pieces[pieceNum].distChange = math.sqrt(1.8*self.track.pieces[pieceNum].cornerValue*cornerRadius)
		elseif self.goSlowMo then
			self.track.pieces[pieceNum].distChange = math.sqrt(0.5*self.track.pieces[pieceNum].cornerValue*cornerRadius)
		else
			self.track.pieces[pieceNum].distChange = math.sqrt(self.track.pieces[pieceNum].cornerValue*cornerRadius)
		end
	end
end

function driver:cleanVariables()
	self.curPiece = nil
	self.angle = 0
	self.prevAngle = 0
	self.cornerStart = nil
	self.cornerAngleMax = nil
	self.crashed = false
	self.goSlowMo = false
	self.bumpingCorner = nil
	self.bumpTarget = nil
	self.slowMoTick = nil
end

function driver:saveLogInfo()
	if self.gameTick then
		--print(self.angle)
		--print("Throttle: " .. self.throttle)
		local file = io.open("data.txt", "a")
		local printString = self.gameTick
		printString = printString .. " " .. self.curPiece
		printString = printString .. " " .. self.curVelocity
		printString = printString .. " " .. self.angle
		printString = printString .. " " .. self.throttle
		if self.track.pieces[self.curPiece].angle then
			printString = printString .. " " .. self.track.pieces[self.curPiece].angle
			printString = printString .. " " .. self.track.pieces[self.curPiece].printRadius
		else
			printString = printString .. " " .. 0
			printString = printString .. " " .. 0
		end
		printString = printString .. "\n"
		file:write(printString)
		io.close(file)
		file = nil
	end
end

function driver:saveData(filename)
	local file = io.open(filename, "w")
	local printString = 0
	printString = printString .. "\n"
	for i=2,#self.track.pieces do
		if self.track.pieces[i].cornerValue then
			printString = printString .. self.track.pieces[i].cornerValue
		else
			printString = printString .. 0
		end
		printString = printString .. "\n"
	end
	
	file:write(printString)
	io.close(file)
	file = nil
end

function driver:loadData(filename)
	local file = io.open(filename, "r")
	if not file then
		return
	end
	local val = file:read("*n")
	local ind = 1
	while(val) do
		self.track.pieces[ind].cornerValue = val
		val = file:read("*n")
		ind = ind + 1
	end
	io.close(file)
	file = nil
end

return driver